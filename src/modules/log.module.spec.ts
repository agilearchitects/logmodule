// Lib
import { expect } from "chai";
import { describe, it } from "mocha";

// Module
import { LogModule } from "./log.module";


describe("LogModule", () => {
  it("Should be able to procude log message", () => {
    const errorMessage = `error - Log - Foobar: "Hello World"`;
    const callback = (value: string) => { expect(value).equal(errorMessage); }
    const log = new LogModule();
    log.error({ code: 1, message: "Hello World" })
  });
});