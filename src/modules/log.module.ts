// Libs
import * as fs from "fs";

// Log levels
export enum logLevel {
  ERROR = 1,
  WARNING = 2,
  LOG = 3,
  INFO = 4,
  VERBOSE = 5,
}

// Log message interface
export interface ILogMessage {
  message: string;
  code?: number;
  error?: any;
}

export interface ILog extends ILogMessage {
  level: logLevel;
}

// Handler method type
export type listnerHandler = (log?: ILog) => void;

// Listner interface
export interface IListner {
  level: logLevel;
  handler: listnerHandler;
}

// Container interface for holding listner reference
interface IListnerRegister {
  listner: IListner;
}

// Interface for return value of registered listner
interface IRegisteredListner {
  listner: IListner;
  unsubscribe: () => void;
}

export class LogModule {
  // Listener container
  private listners: IListnerRegister[] = [];

  public constructor(listner?: IListner[]) {
    // Able to register new listners upon constuction
    if (listner !== undefined) {
      this.registerListner(listner);
    }
  }

  public error(log: ILogMessage): void {
    this.addLog({ level: logLevel.ERROR, ...log });
  }

  public warning(log: ILogMessage): void {
    this.addLog({ level: logLevel.WARNING, ...log });
  }

  public log(log: ILogMessage): void {
    this.addLog({ level: logLevel.LOG, ...log });
  }

  public info(log: ILogMessage): void {
    this.addLog({ level: logLevel.INFO, ...log });
  }

  public verbose(log: ILogMessage): void {
    this.addLog({ level: logLevel.VERBOSE, ...log });
  }

  /**
   * Register new log listner
   * @param listner Listner(s) to add
   */
  public registerListner(listner: IListner): IRegisteredListner;
  public registerListner(listner: IListner[]): IRegisteredListner[];
  public registerListner(listner: IListner | IListner[]): IRegisteredListner | IRegisteredListner[] {
    if (listner instanceof Array) {
      return listner.map((listner: IListner) => this.registerListner(listner));
    }

    const listnerRegister = { listner };
    this.listners.push(listnerRegister);
    return {
      listner,
      unsubscribe: () => {
        // Delete listner
        delete listnerRegister.listner;
        // Update listner array by removing any listner that is undefined
        this.listners = this.listners.filter((listnerRegister: IListnerRegister) => listnerRegister.listner !== listnerRegister.listner);
      },
    };
  }

  /**
   * Execute log and trigger all listners matching log level
   * @param log log message to send
   */
  private addLog(log: ILog): void {
    // Call every listner with the right level setting
    this.listners
      .filter((listnerRegister: IListnerRegister) => log.level <= listnerRegister.listner.level)
      .forEach((listnerRegister: IListnerRegister) => listnerRegister.listner.handler(log));
  }
}

/**
 * Parsing date to string
 * @param date Date to parse
 * @return string formated date (YYYY-MM-DD HH:mm:ss mmm)
 */
const dateFormat = (date: Date): string => {
  // Method for adding leading zeros to single numeric value
  const addLeadingZeros = (value: number): string => {
    return `${value <= 9 ? `0` : ""}${value}`;
  };
  // Return date formated
  return `${date.getFullYear()}` +
    `-${addLeadingZeros(date.getMonth() + 1)}` +
    `-${addLeadingZeros(date.getDate())}` +
    ` ${addLeadingZeros(date.getHours())}` +
    `:${addLeadingZeros(date.getMinutes())}` +
    `:${addLeadingZeros(date.getSeconds())}` +
    ` ${date.getMilliseconds()}ms`;
};

// Pre defined listners free of charge
export const listners = {
  /**
   * Log to console with color coding and pretty format
   * @param level The level of log to output in console
   * @param console module (default node native console module)
   * @return log listner interface
   */
  console: (
    level: logLevel = logLevel.INFO,
    consoleModule: typeof console = console,
  ): IListner => {
    return {
      level,
      handler: (log?: ILog): void => {
        if (log === undefined) { return; }
        // Color definitions
        const colors: { [key: string]: string } = {
          error: "\x1b[31m", // red
          warning: "\x1b[33m", // yellow
          log: "\x1b[34m", // blue
          info: "\x1b[32m", // green
          verbose: "\x1b[35m", // magenta
        };

        // Output log with level, code and message
        consoleModule.log(`\x1b[36m${dateFormat(new Date())}\x1b[0m - ${colors[logLevel[log.level].toLowerCase()]}${logLevel[log.level]}\x1b[0m ${log.code !== undefined ? `(code: \x1b[4m${log.code}\x1b[0m) ` : ""}${log.message}`); // tslint:disable-line: no-console, max-line-length
        if (log.error !== undefined) {
          // Output error message and stack if provided
          if (log.error.message !== undefined && log.error.stack !== undefined) {
            consoleModule.error(log.error.message);
            consoleModule.error(log.error.stack);
          }
        }
      },
    };
  },
  /**
   * Log to file with pretty format
   * @param path what file path to write log to
   * @param level At what level to log to
   * @param fs module (default node native fs module)
   * @return log listner interface
   */
  file: (
    path: string,
    level: logLevel = logLevel.ERROR,
    fsModule: typeof fs = fs,
  ): IListner => {
    return {
      level,
      handler: (log?: ILog): void => {
        if (log === undefined) { return; }
        // Append log message to file
        fsModule.writeFileSync(
          path,
          `${dateFormat(new Date())} - ${logLevel[log.level]} ${log.code !== undefined ? `(code: ${log.code}) ` : ""}${log.message}\n`,
          { encoding: "utf8", flag: "a" });
      },
    };
  },
};
